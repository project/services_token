<?php

declare(strict_types=1);

namespace Drupal\services_token_test;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for Services Token functional tests.
 *
 * @see \Drupal\services_token\Tests\Authentication\ServicesTokenTest::testControllerNotCalledBeforeAuth()
 */
class ServicesTokenTestController extends ControllerBase {

  /**
   * Generates a page displaying the name of the current logged in user.
   *
   * @return array{'#markup': string}
   *   A render array with the user name of the current logged in user.
   */
  public function getCurrentUser(): array {
    return ['#markup' => $this->currentUser()->getAccountName()];
  }

  /**
   * Modifies state.
   *
   * @return array{'#markup': string}
   *   A render array with the the text 'Done'.
   *
   * @see \Drupal\services_token\Tests\Authentication\ServicesTokenTest::testControllerNotCalledBeforeAuth()
   */
  public function modifyState(): array {
    $this->state()->set('services_token_test.state.controller_executed', TRUE);
    return ['#markup' => 'Done'];
  }

  /**
   * Indicates whether state was modified.
   *
   * @return array{'#markup': string}
   *   A render array with either the text 'yep' or 'nope'.
   *
   * @see \Drupal\services_token\Tests\Authentication\ServicesTokenTest::testControllerNotCalledBeforeAuth()
   */
  public function readState(): array {
    return [
      '#markup' => $this->state()->get('services_token_test.state.controller_executed') ? 'yep' : 'nope',
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
