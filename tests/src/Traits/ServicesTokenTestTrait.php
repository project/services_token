<?php

declare(strict_types=1);

namespace Drupal\Tests\services_token\Traits;

/**
 * Provides common functionality for Services Token test classes.
 */
trait ServicesTokenTestTrait {

  /**
   * Retrieves a Drupal path or an absolute path using a services token.
   *
   * @param \Drupal\Core\Url|string $path
   *   Drupal path or URL to load into the internal browser.
   * @param string $token
   *   The token to use for authentication.
   * @param mixed[] $options
   *   (optional) Options to be forwarded to the URL generator.
   *
   * @return string
   *   The retrieved HTML string, also available as $this->getRawContent().
   */
  protected function servicesTokenGet($path, $token, array $options = []): string {
    return $this->drupalGet($path, $options, $this->getTokenHeaders($token));
  }

  /**
   * Returns HTTP headers that can be used for basic authentication in Curl.
   *
   * @param string $token
   *   The token to use for authentication.
   *
   * @return array{'Authorization': string}
   *   An array of raw request headers as used by curl_setopt().
   */
  protected function getTokenHeaders($token): array {
    // Set up Curl to use basic authentication with the test user's token.
    return ['Authorization' => 'Basic ' . base64_encode("$token:")];
  }

}
