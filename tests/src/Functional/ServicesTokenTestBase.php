<?php

declare(strict_types=1);

namespace Drupal\Tests\services_token\Functional;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\services_token\Traits\ServicesTokenTestTrait;
use Drupal\services_token\TokenGeneratorInterface;

/**
 * Abstract base class for services token authentication provider tests.
 */
abstract class ServicesTokenTestBase extends BrowserTestBase {

  use ServicesTokenTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'locale',
    'router_test',
    'services_token_test',
    'services_token',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Returns the token generator to be used in this test.
   */
  abstract protected function getTokenGenerator(): TokenGeneratorInterface;

  /**
   * Tests services token authentication.
   */
  public function testServicesToken(): void {
    // Enable page caching.
    $config = $this->config('system.performance');
    $config->set('cache.page.max_age', 300);
    $config->save();

    $account = $this->drupalCreateUser();
    $this->assertNotFalse($account);
    $url = Url::fromRoute('services_token_test.current_user');

    $validToken = $this->getTokenGenerator()->generate((int) $account->id());

    // Ensure we can log in with valid authentication details.
    $this->servicesTokenGet($url, $validToken['token']);
    $this->assertSession()->pageTextContains($account->getAccountName());
    $this->assertSession()->statusCodeEquals(200);

    $this->assertNotNull($this->mink);
    $this->mink->resetSessions();

    if ($this->hasImprovedCacheHeaders()) {
      $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'UNCACHEABLE (request policy)');
    }
    else {
      $this->assertSession()->responseHeaderDoesNotExist('X-Drupal-Cache');
    }
    // Check that Cache-Control is not set to public.
    $this->assertSession()->responseHeaderNotContains('Cache-Control', 'public');

    $time = $this->container->get(TimeInterface::class);
    assert($time instanceof TimeInterface);

    // Ensure that an expired token gives access denied.
    $expiredToken = $this->getTokenGenerator()->generate((int) $account->id(), $time->getRequestTime() - 3600);
    $this->servicesTokenGet($url, $expiredToken['token']);
    $this->assertSession()->pageTextNotContains($account->getAccountName());
    $this->assertSession()->statusCodeEquals(403);

    $this->assertNotNull($this->mink);
    $this->mink->resetSessions();

    // Ensure that the user is prompted to authenticate if they are not yet
    // authenticated and the route only allows token auth.
    $this->drupalGet($url);
    $expectedRealm = $this->config('system.site')->get('name') . ' API';
    $this->assertSession()->responseHeaderEquals('WWW-Authenticate', 'Basic realm="' . $expectedRealm . '"');
    $this->assertSession()->statusCodeEquals(401);

    // Ensure that a route without token auth defined doesn't prompt for auth.
    $this->drupalGet('admin');
    $this->assertSession()->statusCodeEquals(403);

    $account = $this->drupalCreateUser(['access administration pages']);
    $this->assertNotFalse($account);
    $validAdminToken = $this->getTokenGenerator()->generate((int) $account->id());

    // Ensure that a route without token auth defined doesn't allow login.
    $this->servicesTokenGet(Url::fromRoute('system.admin'), $validAdminToken['token']);
    $this->assertSession()->linkNotExists('Log out', 'User is not logged in');
    $this->assertSession()->statusCodeEquals(403);

    $this->assertNotNull($this->mink);
    $this->mink->resetSessions();

    // Ensure that pages already in the page cache aren't returned from page
    // cache if token is provided.
    $url = Url::fromRoute('router_test.10');
    $this->drupalGet($url);
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS');
    $this->servicesTokenGet($url, $validAdminToken['token']);
    if ($this->hasImprovedCacheHeaders()) {
      $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'UNCACHEABLE (request policy)');
    }
    else {
      $this->assertSession()->responseHeaderDoesNotExist('X-Drupal-Cache');
    }
    // Check that Cache-Control is not set to public.
    $this->assertSession()->responseHeaderNotContains('Cache-Control', 'public');
  }

  /**
   * Tests if a comprehensive message is displayed when the route is denied.
   */
  public function testUnauthorizedErrorMessage(): void {
    $account = $this->drupalCreateUser();
    $this->assertNotFalse($account);

    $url = Url::fromRoute('services_token_test.current_user');

    // Case when no credentials are passed, a user friendly access
    // unauthorized message is displayed.
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(401);
    $this->assertSession()->pageTextNotContains('Exception');
    $this->assertSession()->pageTextContains('Log in to access this page.');

    // Case when empty credentials are passed, a user friendly access denied
    // message is displayed.
    $this->servicesTokenGet($url, "");
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');

    $time = $this->container->get(TimeInterface::class);
    assert($time instanceof TimeInterface);

    // Case when wrong credentials are passed, a user friendly access denied
    // message is displayed.
    $expiredToken = $this->getTokenGenerator()->generate((int) $account->id(), $time->getRequestTime() - 3600);
    $this->servicesTokenGet($url, $expiredToken['token']);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');

    // Case when correct credentials but hasn't access to the route, an user
    // friendly access denied message is displayed.
    $url = Url::fromRoute('router_test.15');
    $validToken = $this->getTokenGenerator()->generate((int) $account->id());
    $this->servicesTokenGet($url, $validToken['token']);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
  }

  /**
   * Tests if the controller is called before authentication.
   *
   * @see https://www.drupal.org/node/2817727
   */
  public function testControllerNotCalledBeforeAuth(): void {
    $this->drupalGet('/services-token-test/state/modify');
    $this->assertSession()->statusCodeEquals(401);
    $this->drupalGet('/services-token-test/state/read');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('nope');

    $account = $this->drupalCreateUser();
    $this->assertNotFalse($account);

    $validToken = $this->getTokenGenerator()->generate((int) $account->id());
    $this->servicesTokenGet('/services-token-test/state/modify', $validToken['token']);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Done');

    $this->assertNotNull($this->mink);
    $this->mink->resetSessions();

    $this->drupalGet('/services-token-test/state/read');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('yep');
  }

  /**
   * Returns true if core has improved cache headers.
   *
   * @see https://www.drupal.org/i/2951814
   */
  protected function hasImprovedCacheHeaders(string $coreVersion = \Drupal::VERSION): bool {
    return match (TRUE) {
      version_compare($coreVersion, '10.4.0') < 0 => FALSE,
      version_compare($coreVersion, '11.0.0') < 0 => TRUE,
      version_compare($coreVersion, '11.1.0') < 0 => FALSE,
      default => TRUE,
    };
  }

}
