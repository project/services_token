<?php

declare(strict_types=1);

namespace Drupal\Tests\services_token\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for services_token.
 *
 * @group services_token
 */
class GenericTest extends GenericModuleTestBase {}
