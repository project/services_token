<?php

declare(strict_types=1);

namespace Drupal\Tests\services_token\Functional;

use Drupal\services_token\TokenGeneratorInterface;

/**
 * Tests for services token authentication provider.
 *
 * @group services_token
 */
class ServicesTokenTest extends ServicesTokenTestBase {

  /**
   * {@inheritdoc}
   */
  protected function getTokenGenerator(): TokenGeneratorInterface {
    $tokenGenerator = $this->container->get(TokenGeneratorInterface::class);
    assert($tokenGenerator instanceof TokenGeneratorInterface);
    return $tokenGenerator;
  }

}
