<?php

declare(strict_types=1);

namespace Drupal\Tests\services_token\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Url;
use Drupal\Tests\rest\Functional\BasicAuthResourceTestTrait;
use Drupal\Tests\rest\Functional\ResourceTestBase;
use Drupal\services_token\RealmResolverInterface;
use Drupal\services_token\SecurityKeyInterface;
use GuzzleHttp\RequestOptions;

/**
 * Tests the services token generate resource.
 *
 * @group services_token
 */
class TokenGenerateResourceTest extends ResourceTestBase {

  use BasicAuthResourceTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $auth = 'basic_auth';

  /**
   * {@inheritdoc}
   */
  protected static $resourceConfigId = 'services_token.generate';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['basic_auth', 'rest', 'services_token'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->provisionResource(
      formats: [static::$format],
      authentication: [static::$auth],
      methods: ['POST'],
    );
  }

  /**
   * Generates an authentication token via the REST API.
   */
  public function testTokenGenerate(): void {
    $this->initAuthentication();

    $url = Url::fromRoute('rest.services_token.generate.POST')
      ->setOption('query', ['_format' => static::$format]);
    $request_options = $this->getAuthenticationRequestOptions('POST');
    $request_options[RequestOptions::HEADERS]['Content-Type'] = static::$mimeType;

    // DX: 403 when unauthorized.
    $response = $this->request('POST', $url, $request_options);
    $this->assertResourceErrorResponse(
      expected_status_code: 403,
      expected_message: $this->getExpectedUnauthorizedAccessMessage('POST'),
      response: $response
    );

    // Grant user account the required permissions to generate a services token
    // via the REST API.
    $this->setUpAuthorization('POST');

    // 201 for well-formed request.
    $response = $this->request('POST', $url, $request_options);
    $this->assertResourceResponse(
      expected_status_code: 201,
      expected_body: FALSE,
      response: $response
    );
    $this->assertFalse($response->hasHeader('X-Drupal-Cache'));

    // Verify token.
    $record = Json::decode((string) $response->getBody());
    $this->assertIsArray($record);
    $this->assertIsString($record['token']);

    $realmResolver = $this->container->get(RealmResolverInterface::class);
    assert($realmResolver instanceof RealmResolverInterface);
    $securityKey = $this->container->get(SecurityKeyInterface::class);
    assert($securityKey instanceof SecurityKeyInterface);

    $realm = $realmResolver->resolveRealm();
    $this->assertTrue($securityKey->verify($record['token'], $realm));
  }

  /**
   * {@inheritdoc}
   */
  protected function setUpAuthorization($method): void {
    switch ($method) {
      case 'POST':
        $this->grantPermissionsToTestedRole(['restful post services_token:generate']);
        break;

      default:
        throw new \UnexpectedValueException();
    }
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $request_options
   */
  protected function assertNormalizationEdgeCases($method, Url $url, array $request_options): void {}

  /**
   * {@inheritdoc}
   */
  protected function getExpectedUnauthorizedAccessCacheability(): RefinableCacheableDependencyInterface {
    return new CacheableMetadata();
  }

}
