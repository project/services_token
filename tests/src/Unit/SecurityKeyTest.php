<?php

declare(strict_types=1);

namespace Drupal\Tests\services_token\Unit;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\services_token\SecurityKey;
use Prophecy\Argument;

/**
 * Tests generation and validation of tokens.
 *
 * @coversDefaultClass \Drupal\$securityKey->generate\SecurityKey
 *
 * @group services_token
 */
class SecurityKeyTest extends UnitTestCase {

  /**
   * The module handler.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy<\Drupal\Core\Extension\ModuleHandlerInterface>
   */
  protected $moduleHandler;

  /**
   * The request time.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy<\Drupal\Component\Datetime\TimeInterface>
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->moduleHandler = $this->prophesize(ModuleHandlerInterface::class);
    $this->time = $this->prophesize(TimeInterface::class);
  }

  /**
   * Tests generating tokens.
   */
  public function testTokenGeneration(): void {
    $uid = mt_rand(1, 0xFFFE);
    $hmacKey = $this->randomMachineName(24);
    $realm = $this->randomMachineName(8);
    $expire = mt_rand(0, 0xFFFFFFFE);

    $securityKey = new SecurityKey(
      $this->time->reveal(),
      $this->moduleHandler->reveal(),
      $hmacKey
    );

    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $token1 = $securityKey->generate($uid, $expire, $realm);

    [$hex_uid, $hex_expire, $hmac] = explode('.', $token1);
    $this->assertEquals($uid, hexdec($hex_uid), 'User id is part of the token');
    $this->assertEquals($expire, hexdec($hex_expire), 'Expiry timestamp is part of the token');
    $this->assertSame(43, strlen($hmac), 'Generated hmac is exactly 43 characters long');

    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $token2 = $securityKey->generate($uid, $expire, $realm);
    $this->assertSame($token1, $token2, 'Token generation is idempotent');

    $realm = $this->randomMachineName(8);
    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $token3 = $securityKey->generate($uid, $expire, $realm);
    $this->assertNotEquals($token1, $token3, 'Generated token varies for different realms');

    $uid++;
    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $token4 = $securityKey->generate($uid, $expire, $realm);
    $this->assertNotEquals($token3, $token4, 'Generated token varies for different users');

    $expire++;
    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $token5 = $securityKey->generate($uid, $expire, $realm);
    $this->assertNotEquals($token4, $token5, 'Generated token varies for different expiry times');
  }

  /**
   * Tests token validation.
   */
  public function testTokenValidation(): void {
    $uid = mt_rand(1, 0xFFFE);
    $requestTime = mt_rand(1, 0xFFFFFFFE);
    $hmacKey = $this->randomMachineName(24);
    $realm = $this->randomMachineName(8);

    $this->time->getRequestTime()->willReturn($requestTime);

    $securityKey = new SecurityKey(
      $this->time->reveal(),
      $this->moduleHandler->reveal(),
      $hmacKey
    );

    $realm = $this->randomMachineName(8);
    $expire = mt_rand($requestTime + 1, 0xFFFFFFFE);
    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $token = $securityKey->generate($uid, $expire, $realm);

    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $result = $securityKey->verify($token, $realm);
    $this->assertSame($result, TRUE, 'Validation succeeds if parameters are the same');

    $parts = explode('.', $token);
    $parts[2] = $this->randomMachineName(24);
    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $result = $securityKey->verify(implode('.', $parts), $realm);
    $this->assertSame($result, FALSE, 'Validation fails if hmac is garbage');

    $parts = explode('.', $token);
    $parts[0] = dechex($uid + 1);
    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $result = $securityKey->verify(implode('.', $parts), $realm);
    $this->assertSame($result, FALSE, 'Validation fails if account is different');

    $parts = explode('.', $token);
    $parts[1] = dechex($expire + 1);
    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $result = $securityKey->verify(implode('.', $parts), $realm);
    $this->assertSame($result, FALSE, 'Validation fails if expiry date is different');

    $other_realm = $this->randomMachineName(7);
    $this->moduleHandler->invokeAll('services_token_properties', Argument::type('array'))->willReturn([
      'uid' => $uid,
      'realm' => $other_realm,
    ]);
    $this->moduleHandler->alter('services_token_properties', Argument::type('array'), Argument::type('int'), Argument::type('string'))->willReturn(NULL);
    $result = $securityKey->verify($token, $other_realm);
    $this->assertSame($result, FALSE, 'Validation fails if realm is different');
  }

  /**
   * Tests token expiry.
   */
  public function testTokenExpiry(): void {
    $uid = mt_rand(1, 0xFFFE);
    $requestTime = mt_rand(1, 0xFFFFFFFE);
    $hmacKey = $this->randomMachineName(24);
    $realm = $this->randomMachineName(8);

    $this->time->getRequestTime()->willReturn($requestTime);

    $securityKey = new SecurityKey(
      $this->time->reveal(),
      $this->moduleHandler->reveal(),
      $hmacKey
    );

    $expire = $requestTime + 1;
    $token = $securityKey->generate($uid, $expire, $realm);
    $result = $securityKey->verify($token, $realm);
    $this->assertSame($result, TRUE, 'Token is valid if expiry time is in the future');

    $expire = $requestTime;
    $token = $securityKey->generate($uid, $expire, $realm);
    $result = $securityKey->verify($token, $realm);
    $this->assertSame($result, FALSE, 'Token is invalid if expiry time is reached');

    $expire = $requestTime - 1;
    $token = $securityKey->generate($uid, $expire, $realm);
    $result = $securityKey->verify($token, $realm);
    $this->assertSame($result, FALSE, 'Token is invalid if expiry time passed');
  }

}
