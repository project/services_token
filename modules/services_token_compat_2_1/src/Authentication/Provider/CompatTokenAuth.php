<?php

declare(strict_types=1);

namespace Drupal\services_token_compat_2_1\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderChallengeInterface;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\services_token\Authentication\Provider\TokenAuth;
use Drupal\services_token\RealmResolverInterface;
use Drupal\services_token\SecurityKeyInterface;
use Drupal\services_token_compat_2_1\CompatSecurityKey;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\Attribute\AutowireDecorated;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Services token authentication provider.
 */
class CompatTokenAuth extends TokenAuth implements AuthenticationProviderInterface, AuthenticationProviderChallengeInterface {

  /**
   * Constructs a compat token authentication provider object.
   *
   * @param \Drupal\Core\Authentication\AuthenticationProviderInterface&\Drupal\Core\Authentication\AuthenticationProviderChallengeInterface $inner
   *   The decorated authentication provider.
   * @param \Drupal\services_token\SecurityKeyInterface $securityKey
   *   The security key.
   * @param \Drupal\services_token\RealmResolverInterface $realmResolver
   *   The realm resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    #[AutowireDecorated]
    protected AuthenticationProviderInterface&AuthenticationProviderChallengeInterface $inner,
    #[Autowire(service: CompatSecurityKey::class)]
    SecurityKeyInterface $securityKey,
    RealmResolverInterface $realmResolver,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($securityKey, $realmResolver, $entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request): bool {
    return $this->inner->applies($request) || parent::applies($request);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request): ?AccountInterface {
    return $this->inner->authenticate($request) ?? parent::authenticate($request);
  }

  /**
   * {@inheritdoc}
   */
  public function challengeException(Request $request, \Exception $previous): ?HttpExceptionInterface {
    return $this->inner->challengeException($request, $previous) ?? parent::challengeException($request, $previous);
  }

}
