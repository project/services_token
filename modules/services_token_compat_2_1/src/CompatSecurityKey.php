<?php

declare(strict_types=1);

namespace Drupal\services_token_compat_2_1;

use Drupal\services_token\SecurityKeyBase;
use Drupal\services_token\SecurityKeyInterface;
use Drupal\user\Entity\User;

/**
 * Provides methods to generate and check the HMAC for compat tokens.
 */
class CompatSecurityKey extends SecurityKeyBase implements SecurityKeyInterface {

  /**
   * {@inheritdoc}
   */
  protected function getTokenProperties(int $uid, string $realm): array {
    $properties = (array) $this->moduleHandler->invokeAll(
      'services_token_properties',
      [$uid, $realm]
    );

    // Replace boolean typed status field with original status value. This was
    // the default before version 2.1.
    if (isset($properties['status']) && is_bool($properties['status'])) {
      $account = User::load($uid);
      $properties['status'] = $account?->status->value;
    }

    $this->moduleHandler->alter(
      'services_token_properties',
      $properties,
      $uid,
      $realm
    );

    return $properties;
  }

}
