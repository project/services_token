<?php

declare(strict_types=1);

namespace Drupal\Tests\services_token_compat_2_1\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for services_token_compat_2_1.
 *
 * @group services_token
 */
class GenericTest extends GenericModuleTestBase {}
