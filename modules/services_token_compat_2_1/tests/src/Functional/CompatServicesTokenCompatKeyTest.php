<?php

declare(strict_types=1);

namespace Drupal\Tests\services_token_compat_2_1\Functional;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\services_token\RealmResolverInterface;
use Drupal\services_token\TokenGenerator;
use Drupal\services_token\TokenGeneratorInterface;
use Drupal\services_token_compat_2_1\CompatSecurityKey;
use Drupal\Tests\services_token\Functional\ServicesTokenTestBase;

/**
 * Tests for compat token authentication provider.
 *
 * @group services_token
 */
class CompatServicesTokenCompatKeyTest extends ServicesTokenTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'services_token_compat_2_1',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getTokenGenerator(): TokenGeneratorInterface {
    $realmResolver = $this->container->get(RealmResolverInterface::class);
    assert($realmResolver instanceof RealmResolverInterface);
    return new TokenGenerator(
      securityKey: $this->container->get(CompatSecurityKey::class),
      currentUser: $this->container->get(AccountInterface::class),
      realmResolver: $realmResolver,
      time: $this->container->get(TimeInterface::class),
      moduleHandler: $this->container->get(ModuleHandlerInterface::class),
    );
  }

}
