<?php

declare(strict_types=1);

namespace Drupal\services_token;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;

/**
 * Default implementation for realm resolver.
 */
class RealmResolver implements RealmResolverInterface {

  /**
   * Constructs a realm resolver.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function resolveRealm(): string {
    $preferredRealm = Settings::get('services_token_realm');
    if (is_string($preferredRealm)) {
      return $preferredRealm;
    }
    else {
      return $this->configFactory->get('system.site')->get('name') . ' API';
    }
  }

}
