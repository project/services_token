<?php

declare(strict_types=1);

namespace Drupal\services_token;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;

/**
 * Default token generator implementation.
 */
class TokenGenerator implements TokenGeneratorInterface {

  /**
   * Default services token TTL.
   */
  const TOKEN_TTL_DEFAULT = 2592000;

  /**
   * Creates a new services token generator.
   *
   * @param \Drupal\services_token\SecurityKeyInterface $securityKey
   *   The security key.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\services_token\RealmResolverInterface $realmResolver
   *   The realm resolver.
   * @param \Drupal\Component\Datetime\Time $time
   *   The current request time.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    protected SecurityKeyInterface $securityKey,
    protected AccountInterface $currentUser,
    protected RealmResolverInterface $realmResolver,
    protected TimeInterface $time,
    protected ModuleHandlerInterface $moduleHandler,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function generate(?int $uid = NULL, ?int $expire = NULL, ?string $realm = NULL): array {
    if (!isset($uid)) {
      $uid = (int) $this->currentUser->id();
    }

    if (!isset($expire)) {
      $ttl = Settings::get('services_token_ttl', self::TOKEN_TTL_DEFAULT);
      $expire = $this->time->getRequestTime() + $ttl;
    }

    if (!isset($realm)) {
      $realm = $this->realmResolver->resolveRealm();
    }

    $this->moduleHandler->alter('services_token_expires', $expire, $uid, $realm);

    $resource = [
      'expires' => date(DATE_ATOM, $expire),
      'token' => $this->securityKey->generate($uid, $expire, $realm),
    ];

    $this->moduleHandler->alter('services_token_create', $resource, $uid, $realm);

    return $resource;
  }

}
