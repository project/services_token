<?php

declare(strict_types=1);

namespace Drupal\services_token;

/**
 * Provides methods to generate and check the HMAC for services tokens.
 */
class SecurityKey extends SecurityKeyBase implements SecurityKeyInterface {

  /**
   * {@inheritdoc}
   */
  protected function getTokenProperties(int $uid, string $realm): array {
    $properties = (array) $this->moduleHandler->invokeAll(
      'services_token_properties',
      [$uid, $realm]
    );

    $this->moduleHandler->alter(
      'services_token_properties',
      $properties,
      $uid,
      $realm
    );

    return $properties;
  }

}
