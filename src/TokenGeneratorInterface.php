<?php

declare(strict_types=1);

namespace Drupal\services_token;

/**
 * Defines an interface for token generator implementations.
 */
interface TokenGeneratorInterface {

  /**
   * Creates and returns an authentication token record.
   *
   * @param int $uid
   *   (optional) The user account id the token should be generated for.
   *   Defaults to the currently logged in user.
   * @param int $expires
   *   (optional) The Unix timestamp of the time when the token will expire.
   *   Defaults to a timestamp computed based on the value of the
   *   services_token_ttl variable.
   * @param string $realm
   *   (optional) A string defining a group of resources this token is valid
   *   for. A site-wide default is selected if omitted.
   *
   * @return array{'expires': string, 'token': string}
   *   A token record with the following key-value pairs:
   *   - expires: The expiry date formatted according to ISO8601
   *   - token: A string which can be used in place of the basic auth username
   *     on subsequent requests.
   */
  public function generate(?int $uid = NULL, ?int $expires = NULL, ?string $realm = NULL): array;

}
