<?php

declare(strict_types=1);

namespace Drupal\services_token\PageCache;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cache policy for pages authenticated by token auth.
 *
 * This policy disallows caching of requests that use token authentication for
 * security reasons. Otherwise responses for authenticated requests can get
 * into the page cache and could be delivered to unprivileged users.
 */
class DisallowTokenAuthRequests implements RequestPolicyInterface {

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    $token = $request->headers->get('PHP_AUTH_USER');
    return isset($token) ? self::DENY : NULL;
  }

}
