<?php

declare(strict_types=1);

namespace Drupal\services_token\Authentication\Provider;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Authentication\AuthenticationProviderChallengeInterface;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\services_token\RealmResolverInterface;
use Drupal\services_token\SecurityKeyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Services token authentication provider.
 */
class TokenAuth implements AuthenticationProviderInterface, AuthenticationProviderChallengeInterface {

  /**
   * Constructs a services token authentication provider object.
   *
   * @param \Drupal\services_token\SecurityKeyInterface $securityKey
   *   The security key.
   * @param \Drupal\services_token\RealmResolverInterface $realmResolver
   *   The realm resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    protected SecurityKeyInterface $securityKey,
    protected RealmResolverInterface $realmResolver,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request) {
    $token = $request->headers->get('PHP_AUTH_USER');
    $password = $request->headers->get('PHP_AUTH_PW');
    return isset($token) && empty($password);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    $token = $request->headers->get('PHP_AUTH_USER');
    assert($token !== NULL);
    $realm = $this->realmResolver->resolveRealm();

    if ($this->securityKey->verify($token, $realm)) {
      [$hex_uid] = explode('.', $token, 2);
      $uid = (int) hexdec($hex_uid);
      return $this->entityTypeManager->getStorage('user')->load($uid);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function challengeException(Request $request, \Exception $previous) {
    $realm = $this->realmResolver->resolveRealm();
    $challenge = new FormattableMarkup('Basic realm="@realm"', [
      '@realm' => $realm,
    ]);

    return new UnauthorizedHttpException(
      (string) $challenge,
      'No authentication credentials provided.',
      $previous
    );
  }

}
