<?php

declare(strict_types=1);

namespace Drupal\services_token\Plugin\rest\resource;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rest\Attribute\RestResource;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\services_token\TokenGeneratorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Token generate rest resource.
 */
#[RestResource(
  id: 'services_token:generate',
  label: new TranslatableMarkup('Generate services token'),
  uri_paths: [
    'create' => '/services_token/generate',
  ],
)]
class TokenGenerateResource extends ResourceBase {

  /**
   * Constructs a token generate resource.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param string[] $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\services_token\TokenGeneratorInterface $tokenGenerator
   *   The token generator service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    protected TokenGeneratorInterface $tokenGenerator,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $configuration
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $loggerFactory = $container->get(LoggerChannelFactoryInterface::class);
    assert($loggerFactory instanceof LoggerChannelFactoryInterface);

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $loggerFactory->get('rest'),
      $container->get(TokenGeneratorInterface::class),
    );
  }

  /**
   * Generates and returns an authentication token.
   */
  public function post(): ModifiedResourceResponse {
    $result = $this->tokenGenerator->generate();
    return new ModifiedResourceResponse($result, 201);
  }

}
