<?php

declare(strict_types=1);

namespace Drupal\services_token\Plugin\ServiceDefinition;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\services\ServiceDefinitionBase;
use Drupal\services_token\TokenGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Token generate service definition.
 *
 * @ServiceDefinition(
 *   id = "services_token_generate",
 *   methods = {
 *     "POST"
 *   },
 *   title = @Translation("Generate services token"),
 *   description = @Translation("Generate a service token and return the token record."),
 *   category = @Translation("Services Token"),
 *   path = "services_token/generate"
 * )
 */
class TokenGenerate extends ServiceDefinitionBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a token generate services.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\services_token\TokenGeneratorInterface $tokenGenerator
   *   The services token generator.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
    protected TokenGeneratorInterface $tokenGenerator,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $configuration
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get(TokenGeneratorInterface::class),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processRoute(Route $route): void {
    $route->setRequirement('_permission', 'generate services token');
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return mixed[]
   */
  public function processRequest(
    Request $request,
    RouteMatchInterface $route_match,
    SerializerInterface $serializer,
  ): array {
    return $this->tokenGenerator->generate();
  }

}
