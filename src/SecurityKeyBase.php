<?php

declare(strict_types=1);

namespace Drupal\services_token;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\PrivateKey;
use Drupal\Core\Site\Settings;

/**
 * Provides base class to generate and check the HMAC for services tokens.
 */
abstract class SecurityKeyBase implements SecurityKeyInterface {

  /**
   * Constructs a services token security key service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The request time service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param string|null $hmacKey
   *   The HMAC key. Defaults services_token_private_key setting and
   *   falls back to Drupal private key + hash salt.
   */
  public function __construct(
    protected TimeInterface $time,
    protected ModuleHandlerInterface $moduleHandler,
    protected ?string $hmacKey = NULL,
  ) {
    if (!isset($this->hmacKey)) {
      $preferredKey = Settings::get('services_token_private_key');
      if (is_string($preferredKey)) {
        $this->hmacKey = $preferredKey;
      }
      else {
        $fallbackKey = \Drupal::service(PrivateKey::class)->get() . Settings::getHashSalt();
        $this->hmacKey = $fallbackKey;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function verify(string $supplied_token, string $realm): bool {
    $result = FALSE;

    if (substr_count($supplied_token, '.') === 2) {
      [$hex_uid, $hex_expire] = explode('.', $supplied_token);
      $uid = (int) hexdec($hex_uid);
      $expire = (int) hexdec($hex_expire);

      $computed_token = $this->generate($uid, $expire, $realm);

      $result = hash_equals($computed_token, $supplied_token) &&
        $this->time->getRequestTime() < $expire;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(int $uid, int $expire, string $realm): string {
    $properties = $this->getTokenProperties($uid, $realm);
    ksort($properties);
    $data = serialize($properties);

    $hmac = Crypt::hmacBase64($uid . $data . $expire, $this->hmacKey);

    return implode(".", [dechex($uid), dechex($expire), $hmac]);
  }

  /**
   * Returns a set of key-value pairs representing the source for tokens.
   *
   * @param int $uid
   *   The user id the token should be generated for.
   * @param string $realm
   *   A string defining a group of resources this token is valid for. Typically
   *   an URL, e.g., http://example.com/api.
   *
   * @return array<string, mixed>
   *   A set of key-value pairs representing the unique exact state of the given
   *   user account.
   */
  abstract protected function getTokenProperties(int $uid, string $realm): array;

}
