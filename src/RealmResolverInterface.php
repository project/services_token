<?php

declare(strict_types=1);

namespace Drupal\services_token;

/**
 * Determines the realm to be used to generate and authenticate tokens.
 */
interface RealmResolverInterface {

  /**
   * Returns the realm to be used to generate / authenticate tokens.
   */
  public function resolveRealm(): string;

}
