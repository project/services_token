<?php

declare(strict_types=1);

namespace Drupal\services_token;

/**
 * Provides methods to generate and check the HMAC for services tokens.
 */
interface SecurityKeyInterface {

  /**
   * Validates a services token.
   *
   * @param string $supplied_token
   *   The services token as supplied by the client.
   * @param string $realm
   *   A string defining a group of resources this token is valid for. Typically
   *   an URL, e.g., http://example.com/api.
   *
   * @return bool
   *   TRUE if this download link is still valid, FALSE otherwise.
   */
  public function verify(string $supplied_token, string $realm): bool;

  /**
   * Calculates a base-64 encoded, URL-safe services token with a sha-256 HMAC.
   *
   * @param int $uid
   *   The user id the token should be generated for.
   * @param int $expire
   *   The Unix timestamp of the time when the token will expire.
   * @param string $realm
   *   A string defining a group of resources this token is valid for. Typically
   *   an URL, e.g., http://example.com/api.
   *
   * @return string
   *   The services token.
   */
  public function generate(int $uid, int $expire, string $realm): string;

}
